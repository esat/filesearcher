package main.java.com.app;

import main.java.com.model.Match;
import main.java.com.model.SearchFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;


public class FileSearcher {

    private static final int MAX_MATCH_RESULT = 10;
    private static final String SPACE_REGEX = "\\s+";
    private static final String NON_ALPHANUMERIC_REGEX = "[\\W]|_";

    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            throw new IllegalArgumentException();
        }
        final File indexableDirectory = new File(args[0]);
        final List<SearchFile> inMemorySearchFiles = getSearchFiles(indexableDirectory);

        Scanner keyboard = new Scanner(System.in);
        while (true) {
            System.out.print("search>");
            final String line = keyboard.nextLine();
            List<Match> matchList = search(line, inMemorySearchFiles);
            Collections.sort(matchList);

            int count = 0;
            for (Match match : matchList) {
                if (count == MAX_MATCH_RESULT) {
                    break;
                }
                System.out.println(match);
                count++;
            }
        }
    }

    private static List<Match> search(String inputLine, List<SearchFile> searchFiles) {
        List<Match> matches = new ArrayList<Match>();

        Set<String> wordsOfInputLine = getDistinctWords(inputLine);
        if (wordsOfInputLine != null && !wordsOfInputLine.isEmpty()) {
            int inputSize = wordsOfInputLine.size();

            for (SearchFile searchFile : searchFiles) {
                Match match = new Match();
                match.setInput(inputLine);
                match.setSearchFile(searchFile);

                int matchCount = 0;
                for (String inputWord : wordsOfInputLine) {
                    if (searchFile.isMatching(inputWord)) {
                        matchCount++;
                    }
                }
                int ranking = (matchCount * 100) / inputSize;
                match.setRanking(ranking);

                matches.add(match);
            }
        }

        return matches;
    }

    private static List<SearchFile> getSearchFiles(File directory) {
        List<SearchFile> searchFiles = new ArrayList<SearchFile>();

        if (directory != null) {
            File[] files = directory.listFiles();

            if (files == null) {
                System.out.println("There cant find any files under " + directory.getPath());
                return searchFiles;
            }
            for (File file : files) {
                if (file != null) {
                    String fileAsString = getFileAsString(file);
                    Set<String> distinctWordsOfCurrentFile = getDistinctWords(fileAsString);

                    SearchFile searchFile = new SearchFile();
                    searchFile.setFileDistinctWords(distinctWordsOfCurrentFile);
                    searchFile.setFileName(file.getName());

                    searchFiles.add(searchFile);
                }
            }
        }

        return searchFiles;
    }

    // a word in string is alphanumeric chars group which seperated by spaces
    private static Set<String> getDistinctWords(String str) {
        Set<String> distinctWords = new HashSet<String>();
        if (str != null && !str.isEmpty()) {
            str = replaceNonAlphanumericChars(str, " ");
            str = str.toLowerCase();
            String[] words = str.split(SPACE_REGEX);
            List<String> wordsAsList = Arrays.asList(words);
            distinctWords = new HashSet<String>(wordsAsList);
        }

        return distinctWords;
    }

    private static String replaceNonAlphanumericChars(String str, String replacingStr) {
        if (str != null && !str.isEmpty()) {
            str = str.replaceAll(NON_ALPHANUMERIC_REGEX, replacingStr);
        }
        return str;
    }

    private static String getFileAsString(File file) {
        String fileAsString = null;

        if (file != null) {
            try {
                byte[] fileAsBtyes = Files.readAllBytes(file.toPath());
                fileAsString = new String(fileAsBtyes);
            } catch (IOException e) {
                System.out.println("There is an unexpected error while reading the file: " + file.getName());
            }
        }

        return fileAsString;
    }

}
