package main.java.com.model;

public class Match implements Comparable<Match> {
    private String input;
    private SearchFile searchFile;
    private int ranking;

    public SearchFile getSearchFile() {
        return searchFile;
    }

    public void setSearchFile(SearchFile searchFile) {
        this.searchFile = searchFile;
    }

    public int getRanking() {
        return ranking;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    @Override
    public int compareTo(Match comparingMatch) {
        return (comparingMatch.getRanking() - this.getRanking());
    }

    @Override
    public String toString() {
        return getSearchFile().getFileName() + " : " + getRanking() + "%";
    }
}
