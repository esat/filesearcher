package main.java.com.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class SearchFile {
    private String fileName;
    private Set<String> fileDistinctWords = new HashSet<String>();

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Set<String> getFileDistinctWords() {
        return fileDistinctWords;
    }

    public void setFileDistinctWords(Set<String> fileDistinctWords) {
        this.fileDistinctWords = fileDistinctWords;
    }

    public Boolean isMatching(String word) {
        Boolean isMatching = Boolean.FALSE;
        if (Objects.nonNull(word) && !word.isEmpty()) {
            isMatching = fileDistinctWords.contains(word);
        }
        return isMatching;
    }
}
